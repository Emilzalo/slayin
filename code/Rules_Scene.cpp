/*
 * GAME SCENE
 * Copyright © 2018+ Ángel Rodríguez Ballesteros
 *
 * Distributed under the Boost Software License, version  1.0
 * See documents/LICENSE.TXT or www.boost.org/LICENSE_1_0.txt
 *
 * angel.rodriguez@esne.edu
 */

#include "Menu_Scene.hpp"
#include "Rules_Scene.hpp"


#include <cstdlib>
#include <basics/Canvas>
#include <basics/Director>
#include <basics/Log>
#include <basics/Scaling>
#include <basics/Rotation>
#include <basics/Translation>

using namespace basics;
using namespace std;

namespace example
{

    Rules_Scene::Texture_Data Rules_Scene::textures_data[] =
    {
        { ID(loading),    "loading.png" },
        { ID(backButton),  "backButton.png" },
        { ID(ground), "ground.png"},
        { ID(background), "background.png"},
        { ID(rulesText), "rules.png"},



    };

    unsigned Rules_Scene::textures_count = sizeof(textures_data) / sizeof(Texture_Data);

    // ---------------------------------------------------------------------------------------------



    // ---------------------------------------------------------------------------------------------

    Rules_Scene::Rules_Scene()
    {
        canvas_width  = 1280;
        canvas_height =  720;

        srand (unsigned(time(nullptr)));

        initialize ();
    }

    // ---------------------------------------------------------------------------------------------

    bool Rules_Scene::initialize ()
    {
        state    = LOADING;
        paused   = true;
        gameplay = UNINITIALIZED;

        return true;
    }

    // ---------------------------------------------------------------------------------------------

    void Rules_Scene::suspend ()
    {
        paused = true;
    }

    // ---------------------------------------------------------------------------------------------

    void Rules_Scene::resume ()
    {
        paused = false;
    }

    // ---------------------------------------------------------------------------------------------

    void Rules_Scene::handle (Event & event)
    {
        if (state == RUNNING)
        {
            if(gameplay == WAITING_FOR_PLAYER)
            {
                start_playing();
            }
            else switch(event.id)
                {
                    case ID(touch-started):
                    {
                        x = *event[ID(x)].as< var::Float > ();
                        y = *event[ID(y)].as< var::Float > ();

                        if(x > backButton1->get_left_x() && x < backButton1->get_right_x() && y > backButton1->get_bottom_y() && backButton1->get_top_y() ){
                            director.run_scene (shared_ptr< Scene >(new Menu_Scene));
                        }

                    }
                }

        }
    }

    // ---------------------------------------------------------------------------------------------

    void Rules_Scene::update (float time)
    {
        if (!paused) switch (state)
        {
            case LOADING: load_textures  ();     break;
            case RUNNING: run_simulation (time); break;
            case ERROR:   break;
        }
    }

    // ---------------------------------------------------------------------------------------------

    void Rules_Scene::render (basics::Graphics_Context::Accessor & context)
    {
        if (!paused)
        {
            Canvas * canvas = context->get_renderer< Canvas > (ID(canvas));

            if (!canvas)
            {
                 canvas = Canvas::create (ID(canvas), context, {{ canvas_width, canvas_height }});
            }

            if (canvas)
            {
                canvas->clear ();

                switch (state)
                {
                    case LOADING: render_loading   (*canvas); break;
                    case RUNNING: render_scene (*canvas); break;
                    case ERROR:   break;
                }
            }
        }
    }

    // ---------------------------------------------------------------------------------------------

    void Rules_Scene::load_textures ()
    {
        Graphics_Context::Accessor context = director.lock_graphics_context ();

        if (context)
        {
            // Se carga la siguiente textura:

            Texture_Data   & texture_data = textures_data[textures.size ()];
            Texture_Handle & texture      = textures[texture_data.id] = Texture_2D::create (texture_data.id, context, texture_data.path);

            // Se comprueba si la textura se ha podido cargar correctamente:

            if (texture) context->add (texture); else state = ERROR;

            // Se han terminado de cargar todas las texturas:

            if (textures.size () == textures_count)
            {
                create_sprites ();
                restart_game   ();

                state = RUNNING;
            }
        }
    }

    // ---------------------------------------------------------------------------------------------

    void Rules_Scene::create_sprites ()
    {
        // Se crean y configuran los sprites del fondo:

        Sprite_Handle background(new Sprite(textures[ID(background)].get()));
        background->set_anchor(BOTTOM | LEFT);
        background->set_position({0, 0});

        Sprite_Handle ground(new Sprite(textures[ID(ground)].get()));
        ground->set_anchor(BOTTOM | LEFT);
        ground->set_position({0, 0});

        Sprite_Handle backButton(new Sprite(textures[ID(backButton)].get()));
        backButton->set_anchor(CENTER);
        backButton->set_position({ canvas_width / 2.f, canvas_height / 2.f - 200});

        Sprite_Handle rulesText(new Sprite(textures[ID(rulesText)].get()));
        rulesText->set_anchor(CENTER);
        rulesText->set_position({ canvas_width / 2.f, canvas_height / 2.f + 100});


        sprites.push_back(background);
        sprites.push_back(ground);
        sprites.push_back(backButton);
        sprites.push_back(rulesText);

        background1 = background.get();
        ground1 = ground.get();
        backButton1 = backButton.get();
        rulesText1 = rulesText.get();

    }

    // ---------------------------------------------------------------------------------------------
    //Reposiciona todos los sprites
    void Rules_Scene::restart_game()
    {

    }

    // ---------------------------------------------------------------------------------------------

    void Rules_Scene::start_playing ()
    {

        /* Vector2f random_direction
         (
             float(rand () % int(canvas_width ) - int(canvas_width  / 2)),
             float(rand () % int(canvas_height) - int(canvas_height / 2))
         );

         ball->set_speed (random_direction.normalized () * ball_speed);
         */
        gameplay = PLAYING;
    }

    // ---------------------------------------------------------------------------------------------

    void Rules_Scene::run_simulation (float time)
    {
        // Se actualiza el estado de todos los sprites:

        for (auto & sprite : sprites)
        {
            sprite->update (time);
        }

        // Se comprueban las posibles colisiones de la bola con los bordes y con los players:
    }

    // ---------------------------------------------------------------------------------------------



    // ---------------------------------------------------------------------------------------------


   //------------------------------------------------------------------------------------------------


    // ---------------------------------------------------------------------------------------------



    // ---------------------------------------------------------------------------------------------

    void Rules_Scene::render_loading (Canvas & canvas)
    {
        Texture_2D * loading_texture = textures[ID(loading)].get ();

        if (loading_texture)
        {
            canvas.fill_rectangle
            (
                { canvas_width * .5f, canvas_height * .5f },
                { loading_texture->get_width (), loading_texture->get_height () },
                  loading_texture
            );
        }
    }

    // ---------------------------------------------------------------------------------------------

    void Rules_Scene::render_scene(Canvas &canvas)
    {
        for (auto & sprite : sprites)
        {
            sprite->render (canvas);
        }
    }

}
