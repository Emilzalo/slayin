/*
 * GAME SCENE
 * Copyright © 2018+ Ángel Rodríguez Ballesteros
 *
 * Distributed under the Boost Software License, version  1.0
 * See documents/LICENSE.TXT or www.boost.org/LICENSE_1_0.txt
 *
 * angel.rodriguez@esne.edu
 */

#include "Game_Scene.hpp"
#include "Menu_Scene.hpp"

#include <cstdlib>
#include <basics/Canvas>
#include <basics/Director>
#include <basics/Log>
#include <basics/Scaling>
#include <basics/Rotation>
#include <basics/Translation>

using namespace basics;
using namespace std;

namespace example
{

    Game_Scene::Texture_Data Game_Scene::textures_data[] =
    {
        { ID(loading),    "loading.png" },
        { ID(character),  "Character.png" },
        { ID(characterLeft), "characterMirror.png"},
        { ID(ground),     "ground.png" },
        { ID(leftWall),     "wall.png" },
        { ID(rightWall), "wall.png"},
        { ID(background), "background.png"},
        { ID(pauseButton), "pauseButton.png"},
        { ID(exitButton), "exitButton.png"},
        { ID(slime), "slime.png"},
        { ID(gameOverText), "gameOverText.png"},
        { ID(resumeButton), "resumeButton.png"}


    };

    unsigned Game_Scene::textures_count = sizeof(textures_data) / sizeof(Texture_Data);

    // ---------------------------------------------------------------------------------------------

    constexpr float Game_Scene:: player_speed_x;
    constexpr float Game_Scene:: enemy_speed;

    // ---------------------------------------------------------------------------------------------

    Game_Scene::Game_Scene()
    {
        canvas_width  = 1280;
        canvas_height =  720;

        srand (unsigned(time(nullptr)));

       // enemies.resize(enemy_pool_size);

        initialize ();
    }

    // ---------------------------------------------------------------------------------------------

    bool Game_Scene::initialize ()
    {
        state     = LOADING;
        suspended = true;
        gameplay  = UNINITIALIZED;

        return true;
    }

    // ---------------------------------------------------------------------------------------------

    void Game_Scene::suspend ()
    {
        paused = true;
        suspended = true;
    }

    // ---------------------------------------------------------------------------------------------

    void Game_Scene::resume ()
    {
        suspended = false;
    }

    // ---------------------------------------------------------------------------------------------

    void Game_Scene::handle (Event & event)
    {
        if (!suspended && state == RUNNING)
        {
            if(gameplay == WAITING_FOR_PLAYER)
            {
                start_playing();
            }
            else switch(event.id)
                {
                    case ID(touch-started):
                    {
                        x = *event[ID(x)].as< var::Float > ();
                        y = *event[ID(y)].as< var::Float > ();

                        /*if (paused)
                        {
                            touch_on_pause (x, y);
                        }
                        else
                        {
                           touch_while_playing ();
                        }*/
                        if(gameOver){
                            director.run_scene (shared_ptr< Scene >(new Menu_Scene));
                        }

                        if(x > exitButton1->get_left_x() && x < exitButton1->get_right_x() && y > exitButton1->get_bottom_y() && y < exitButton1->get_top_y() && exitActive ){
                            director.run_scene (shared_ptr< Scene >(new Menu_Scene));
                        }

                        if(x > resumeButton1->get_left_x() && x < resumeButton1->get_right_x() && y > resumeButton1->get_bottom_y() && y < resumeButton1->get_top_y() && inPause){
                            inPause = false;
                            exitActive = false;
                            exitButton1->hide();
                            resumeButton1->hide();
                            pauseButton1->show();
                            if(moveRight && !characterIntersect)
                            {
                                character1->set_speed_x(player_speed_x);
                            }
                            else if(!moveRight && !characterIntersect)
                            {
                                characterLeft1->set_speed_x(-player_speed_x);
                            }
                            slime1->set_speed_x(lastSlimeSpeed);

                        }
                        else if(x > pauseButton1->get_left_x() && y > pauseButton1->get_bottom_y()){

                            exitActive = true;
                            exitButton1->show();
                            resumeButton1->show();
                            pauseButton1->hide();
                            if(moveRight){
                                character1->set_speed_x(0);

                            } else{
                                characterLeft1->set_speed_x(0);
                            }

                            slime1->set_speed_x(0);
                            slime1->set_speed_y(0);


                            inPause = true;
                        }

                        else if(!inPause){
                            if(characterLeft1->get_speed_x() == 0 && characterLeft1->intersects(*leftWall1) && !moveRight ) {
                                character1->set_position({characterLeft1->get_position_x(), characterLeft1->get_position_y()});
                                characterLeft1->hide();
                                character1->set_speed_x(player_speed_x);
                                character1->show();
                                moveRight = true;
                            }
                            else if(character1->get_speed_x() == 0 && character1->intersects(*rightWall1) && moveRight){
                                characterLeft1->set_position({character1->get_position_x(), character1->get_position_y()});
                                character1->hide();
                                characterLeft1->show();
                                characterLeft1->set_speed_x(-player_speed_x);
                                moveRight = false;
                            }
                            else if(moveRight){
                                characterLeft1->set_position({character1->get_position_x(), character1->get_position_y()});
                                characterLeft1->set_speed_x(-character1->get_speed_x());
                                character1->hide();
                                characterLeft1->show();
                                moveRight = false;
                            }
                            else{
                                character1->set_position({characterLeft1->get_position_x(), characterLeft1->get_position_y()});
                                character1->set_speed_x(-characterLeft1->get_speed_x());
                                characterLeft1->hide();
                                character1->show();
                                moveRight = true;
                            }

                        }

                    }
                }

        }
    }

    // ---------------------------------------------------------------------------------------------

    void Game_Scene::update (float time)
    {
        if (!paused) switch (state)
        {
            case LOADING: load_textures  ();     break;
            case RUNNING: run_simulation (time); break;
            case ERROR:   break;
        }
    }

    // ---------------------------------------------------------------------------------------------

    void Game_Scene::render (basics::Graphics_Context::Accessor & context)
    {
        if (!paused)
        {
            Canvas * canvas = context->get_renderer< Canvas > (ID(canvas));

            if (!canvas)
            {
                 canvas = Canvas::create (ID(canvas), context, {{ canvas_width, canvas_height }});
            }

            if (canvas)
            {
                canvas->clear ();

                switch (state)
                {
                    case LOADING: render_loading   (*canvas); break;
                    case RUNNING: render_scene (*canvas); break;
                    case ERROR:   break;
                }
            }
        }
    }

    // ---------------------------------------------------------------------------------------------

    void Game_Scene::load_textures ()
    {
        Graphics_Context::Accessor context = director.lock_graphics_context ();

        if (context)
        {
            // Se carga la siguiente textura:

            Texture_Data   & texture_data = textures_data[textures.size ()];
            Texture_Handle & texture      = textures[texture_data.id] = Texture_2D::create (texture_data.id, context, texture_data.path);

            // Se comprueba si la textura se ha podido cargar correctamente:

            if (texture) context->add (texture); else state = ERROR;

            // Se han terminado de cargar todas las texturas:

            if (textures.size () == textures_count)
            {
                create_sprites ();
                restart_game   ();

                state = RUNNING;
            }
        }
    }

    // ---------------------------------------------------------------------------------------------

    void Game_Scene::create_sprites ()
    {
        // Se crean y configuran los sprites del fondo:

       /* Sprite_Handle    top_bar(new Sprite( textures[ID(hbar)].get () ));
        Sprite_Handle middle_bar(new Sprite( textures[ID(vbar)].get () ));
        Sprite_Handle bottom_bar(new Sprite( textures[ID(hbar)].get () ));

           top_bar->set_anchor   (TOP | LEFT);
           top_bar->set_position ({ 0, canvas_height });
        middle_bar->set_anchor   (CENTER);
        middle_bar->set_position ({ canvas_width / 2.f, canvas_height / 2.f });
        bottom_bar->set_anchor   (BOTTOM | LEFT);
        bottom_bar->set_position ({ 0, 0 });

        //Se añaden los sprites

        sprites.push_back (   top_bar);
        sprites.push_back (middle_bar);
        sprites.push_back (bottom_bar);*/

        Sprite_Handle background(new Sprite(textures[ID(background)].get()));
        background->set_anchor(BOTTOM | LEFT);
        background->set_position({0, 0});

        Sprite_Handle ground(new Sprite(textures[ID(ground)].get()));
        ground->set_anchor(BOTTOM | LEFT);
        ground->set_position({0, 0});

        Sprite_Handle leftWall(new Sprite (textures[ID(leftWall)].get()));
        leftWall->set_anchor(LEFT | CENTER);
        leftWall->set_position({0, 500});

        Sprite_Handle rightWall(new Sprite (textures[ID(rightWall)].get()));
        rightWall->set_anchor(RIGHT | CENTER);
        rightWall->set_position({1280, 500});

        Sprite_Handle pauseButton(new Sprite (textures[ID(pauseButton)].get()));
        pauseButton->set_anchor(RIGHT | TOP);
        pauseButton->set_position({1280, 720});
        pauseButton->set_scale(0.7);

        Sprite_Handle gameOverText(new Sprite (textures[ID(gameOverText)].get()));
        gameOverText->set_anchor(CENTER);
        gameOverText->set_position({ canvas_width / 2.f, canvas_height / 2.f});

        Sprite_Handle exitButton(new Sprite (textures[ID(exitButton)].get()));
        exitButton->set_anchor(CENTER);
        exitButton->set_position({ canvas_width / 2.f, canvas_height / 2.f - 100});

        Sprite_Handle resumeButton(new Sprite (textures[ID(resumeButton)].get()));
        resumeButton->set_anchor(CENTER);
        resumeButton->set_position({ canvas_width / 2.f, canvas_height / 2.f + 100});

        sprites.push_back(background);
        sprites.push_back(ground);
        sprites.push_back(leftWall);
        sprites.push_back(rightWall);
        sprites.push_back(pauseButton);
        sprites.push_back(exitButton);
        sprites.push_back(gameOverText);
        sprites.push_back(resumeButton);

        Sprite_Handle character(new Sprite(textures[ID(character)].get()));
        sprites.push_back(character);

        Sprite_Handle characterLeft(new Sprite(textures[ID(characterLeft)].get()));
        sprites.push_back(characterLeft);

        Sprite_Handle slime(new Sprite (textures[ID(slime)].get()));
        sprites.push_back(slime);
        slime->set_scale(0.5);


       /* Sprite_Handle  left_player_handle(new Sprite( textures[ID(player-bar)].get () ));
        Sprite_Handle right_player_handle(new Sprite( textures[ID(player-bar)].get () ));
        Sprite_Handle         ball_handle(new Sprite( textures[ID(ball)      ].get () ));

        sprites.push_back ( left_player_handle);
        sprites.push_back (right_player_handle);
        sprites.push_back (        ball_handle);

        // Se guardan punteros a los sprites que se van a usar frecuentemente:

        top_border    =             top_bar.get ();
        bottom_border =          bottom_bar.get ();
        left_player   =  left_player_handle.get ();
        right_player  = right_player_handle.get ();
        ball          =         ball_handle.get ();*/

        this->background1 = background.get();
        characterLeft1 = characterLeft.get();
        character1 = character.get();
        slime1 = slime.get();
        ground1 = ground.get();
        leftWall1 = leftWall.get();
        rightWall1 = rightWall.get();
        pauseButton1 = pauseButton.get();
        exitButton1 = exitButton.get();
        gameOverText1 = gameOverText.get();
        resumeButton1 = resumeButton.get();

        /*for (auto & enemy : enemies)
        {
            enemy.reset (new Sprite( textures[ID(slime)]);
            enemy->hide();
        }*/
    }

    // ---------------------------------------------------------------------------------------------

    /*void Game_Scene::respawn_enemy ()
    {
        for (auto & enemy : enemies)
        {
            if (enemy->is_not_visible())
            {
                // establecer su nueva posición
                enemy->set_position({(rand () % int(canvas_width)), 0});
                // hacerlo visible
                enemy->show();
                break;
            }
        }
    }*/
    // ---------------------------------------------------------------------------------------------
    //Reposiciona todos los sprites
    void Game_Scene::restart_game()
    {
        moveRight = true;
        character1->set_position({ canvas_width / 2.f, ground1->get_top_y() + 25});
        character1->set_speed_x(player_speed_x);

        slime1->set_position({(rand () % int(canvas_width)), 0});

        ascending = true;
        moving = false;
        inPause = false;
        spawned = true;
        exitActive = false;
        characterLeft1->hide();
        exitButton1->hide();
        gameOver = false;
        gameOverText1->hide();
        resumeButton1->hide();

      /*   left_player->set_position ({ left_player->get_width () * 3.f, canvas_height / 2.f });
         left_player->set_speed_y  (0.f);
        right_player->set_position ({ canvas_width  - right_player->get_width () * 3.f, canvas_height / 2.f });
        right_player->set_speed_y  (0.f);
                ball->set_position ({ canvas_width / 2.f, canvas_height / 2.f });
                ball->set_speed    ({ 0.f, 0.f });

        follow_target = false;

        gameplay = WAITING_TO_START;*/
    }

    // ---------------------------------------------------------------------------------------------

    void Game_Scene::start_playing ()
    {

       /* Vector2f random_direction
        (
            float(rand () % int(canvas_width ) - int(canvas_width  / 2)),
            float(rand () % int(canvas_height) - int(canvas_height / 2))
        );

        ball->set_speed (random_direction.normalized () * ball_speed);
        */

        gameplay = PLAYING;
    }

    // ---------------------------------------------------------------------------------------------

    void Game_Scene::run_simulation (float time)
    {
        if (!paused)
        {
            // Se actualiza el estado de todos los sprites:

            for (auto &sprite : sprites) {
                sprite->update(time);
            }

            for (auto &enemy : enemies) {
                enemy->update(time);
            }

            update_player();
            update_enemy();
            check_collisions();
            game_Over();
        }
        // Se comprueban las posibles colisiones de la bola con los bordes y con los players:
    }

    // ---------------------------------------------------------------------------------------------

    void Game_Scene::update_enemy ()
    {
        if(!spawned){
            slime1->set_position({(rand () % int(canvas_width)), 0});
            slime1->show();
            spawned = true;
            ascending=true;
            moving= false;
        }

        if(ascending){
            slime1->set_speed_y(enemy_speed);
            slime1->set_speed_x(0);
        }

        if(slime1->get_position_y() >= ground1->get_top_y() + 20 && !moving){
            slime1->set_speed_y(0);
            slime1->set_speed_x(enemy_speed);
            moving = true;
            ascending = false;
        }

        else if(moving && (slime1->intersects(*leftWall1) || slime1->intersects(*rightWall1)))
        {
            slime1->set_speed_x(-slime1->get_speed_x());
            lastSlimeSpeed = slime1->get_speed_x();
        }




        /*if (left_player->intersects (*top_border))
        {
            left_player->set_position_y (top_border->get_bottom_y () - left_player->get_height () / 2.f);
            left_player->set_speed_y (0.f);
        }
        else
        if (left_player->intersects (*bottom_border))
        {
            left_player->set_position_y (bottom_border->get_top_y () + left_player->get_height () / 2.f);
            left_player->set_speed_y (0.f);
        }
        else
        {
            float delta_y = ball->get_position_y () - left_player->get_position_y ();

            if (ball->get_speed_y () < 0.f)
            {
                if (delta_y < 0.f)
                {
                    left_player->set_speed_y (-player_speed * (ball->get_speed_x () < 0.f ? 1.f : .5f));
                }
                else
                    left_player->set_speed_y (0.f);
            }
            else
            if (ball->get_speed_y () > 0.f)
            {
                if (delta_y > 0.f)
                {
                    left_player->set_speed_y (+player_speed * (ball->get_speed_x () < 0.f ? 1.f : .5f));
                }
                else
                    left_player->set_speed_y (0.f);
            }
        }*/
    }

    // ---------------------------------------------------------------------------------------------

   /* void Game_Scene::update_user ()
    {
        if (right_player->intersects (*top_border))
        {
            right_player->set_position_y (top_border->get_bottom_y () - right_player->get_height () / 2.f);
            right_player->set_speed_y (0);
        }
        else
        if (right_player->intersects (*bottom_border))
        {
            right_player->set_position_y (bottom_border->get_top_y () + right_player->get_height () / 2.f);
            right_player->set_speed_y (0);
        }
        else
        if (follow_target)
        {
            float delta_y = user_target_y - right_player->get_position_y ();

            if (delta_y < 0.f) right_player->set_speed_y (-player_speed); else
            if (delta_y > 0.f) right_player->set_speed_y (+player_speed);
        }
        else
            right_player->set_speed_y (0);
    }*/

   //------------------------------------------------------------------------------------------------

    void Game_Scene::update_player()
   {

        if(character1->intersects(*rightWall1))
        {
            character1->set_speed_x(0);
            characterIntersect = true;
        } else {
            characterIntersect = false;
        }

        if(characterLeft1->intersects(*leftWall1))
        {
            characterLeft1->set_speed_x(0);
            characterIntersect = true;
        } else{
            characterIntersect = false;
        }


   }

    // ---------------------------------------------------------------------------------------------

    void Game_Scene::check_collisions ()
    {
        if(character1->intersects(*slime1) && character1->get_position_x() < slime1->get_position_x() && character1->contains({slime1->get_position_x(), slime1->get_position_y()})){
            slime1->hide();
            spawned = false;
        }

        if(character1->intersects(*slime1) && character1->get_position_x() > slime1->get_position_x() && character1->contains({slime1->get_position_x(), slime1->get_position_y()})){
            character1->hide();
            gameOver = true;
        }

        if(characterLeft1->intersects(*slime1) && characterLeft1->get_position_x() > slime1->get_position_x() && characterLeft1->contains({slime1->get_position_x(), slime1->get_position_y()})){
            slime1->hide();
            spawned = false;
        }

        if(characterLeft1 ->intersects(*slime1) && characterLeft1->get_position_x() < slime1->get_position_x() && characterLeft1->contains({slime1->get_position_x(), slime1->get_position_y()})){
            characterLeft1->hide();
            gameOver = true;
        }
    }

    // ---------------------------------------------------------------------------------------------
    // ---------------------------------------------------------------------------------------------

    void Game_Scene::game_Over()
    {
        if(gameOver)
        {
            gameOverText1->show();
            slime1->set_speed_x(0);
        }
    }

    // ---------------------------------------------------------------------------------------------

    void Game_Scene::render_loading (Canvas & canvas)
    {
        Texture_2D * loading_texture = textures[ID(loading)].get ();

        if (loading_texture)
        {
            canvas.fill_rectangle
            (
                { canvas_width * .5f, canvas_height * .5f },
                { loading_texture->get_width (), loading_texture->get_height () },
                  loading_texture
            );
        }
    }

    // ---------------------------------------------------------------------------------------------

    void Game_Scene::render_scene(Canvas &canvas)
    {
        for (auto & sprite : sprites)
        {
            sprite->render (canvas);
        }

        for (auto & enemy : enemies)
        {
            enemy->render (canvas);
        }
    }

}
