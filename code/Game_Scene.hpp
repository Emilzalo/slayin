/*
 * GAME SCENE
 * Copyright © 2018+ Ángel Rodríguez Ballesteros
 *
 * Distributed under the Boost Software License, version  1.0
 * See documents/LICENSE.TXT or www.boost.org/LICENSE_1_0.txt
 *
 * angel.rodriguez@esne.edu
 */

#ifndef GAME_SCENE_HEADER
#define GAME_SCENE_HEADER

    #include <map>
    #include <list>
    #include <memory>
    #include <basics/Canvas>
    #include <basics/Id>
    #include <basics/Scene>
    #include <basics/Texture_2D>

    #include "Sprite.hpp"

    namespace example
    {

        using basics::Id;
        using basics::Canvas;
        using basics::Texture_2D;

        class Game_Scene : public basics::Scene
        {

            typedef std::shared_ptr < Sprite     >     Sprite_Handle;
            typedef std::list< Sprite_Handle   >     Sprite_List;
            typedef std::shared_ptr< Texture_2D  >     Texture_Handle;
            typedef std::map< Id, Texture_Handle >     Texture_Map;
            typedef basics::Graphics_Context::Accessor Context;

            enum State
            {
                LOADING,
                RUNNING,
                ERROR
            };

            enum Gameplay_State
            {
                UNINITIALIZED,
                WAITING_FOR_PLAYER,
                PLAYING,
            };

        private:

            static struct   Texture_Data { Id id; const char * path; } textures_data[];
            static unsigned textures_count;

        private:

            static constexpr float player_speed_x = 120.f;
            static constexpr float enemy_speed = 120.f;

           // static const size_t enemy_pool_size  = 20;

        private:

            State          state;
            Gameplay_State gameplay;
            bool           suspended;
            bool           paused;

            bool inPause;

            unsigned       canvas_width;
            unsigned       canvas_height;

            Texture_Map    textures;
            Sprite_List    sprites;
            Sprite_List    enemies;

            Sprite  * character1;
            Sprite  * characterLeft1;
            Sprite  * ground1;
            Sprite  * leftWall1;
            Sprite  * rightWall1;
            Sprite  * background1;
            Sprite  * pauseButton1;
            Sprite  * exitButton1;
            Sprite  * slime1;
            Sprite  * gameOverText1;
            Sprite  * resumeButton1;



        public:

            Game_Scene();

            basics::Size2u get_view_size () override
            {
                return { canvas_width, canvas_height };
            }


            bool characterIntersect;
            bool moveRight;
            bool exitActive;
            bool spawned;
            bool ascending;
            bool moving;
            bool gameOver;
            float x;
            float y;
            float lastSlimeSpeed;


            bool initialize () override;
            void suspend    () override;
            void resume     () override;

            void handle     (basics::Event & event) override;
            void update     (float time) override;
            void render     (Context & context) override;

        private:

            void load_textures    ();
            void create_sprites   ();
         //   void respawn_enemy    ();

            void restart_game     ();
            void start_playing    ();

            void run_simulation   (float time);
            void update_player    ();
            void check_collisions ();
            void update_enemy     ();
            void game_Over         ();

            void render_loading   (Canvas & canvas);
            void render_scene     (Canvas & canvas);


        };

    }

#endif
