/*
 * GAME SCENE
 * Copyright © 2018+ Ángel Rodríguez Ballesteros
 *
 * Distributed under the Boost Software License, version  1.0
 * See documents/LICENSE.TXT or www.boost.org/LICENSE_1_0.txt
 *
 * angel.rodriguez@esne.edu
 */

#ifndef RULES_SCENE_HEADER
#define RULES_SCENE_HEADER

    #include <map>
    #include <list>
    #include <memory>
    #include <basics/Canvas>
    #include <basics/Id>
    #include <basics/Scene>
    #include <basics/Texture_2D>

    #include "Sprite.hpp"

    namespace example
    {

        using basics::Id;
        using basics::Canvas;
        using basics::Texture_2D;

        class Rules_Scene : public basics::Scene
        {

            typedef std::shared_ptr < Sprite     >     Sprite_Handle;
            typedef std::list< Sprite_Handle     >     Sprite_List;
            typedef std::shared_ptr< Texture_2D  >     Texture_Handle;
            typedef std::map< Id, Texture_Handle >     Texture_Map;
            typedef basics::Graphics_Context::Accessor Context;

            enum State
            {
                LOADING,
                RUNNING,
                ERROR
            };

            enum Gameplay_State
            {
                UNINITIALIZED,
                WAITING_FOR_PLAYER,
                PLAYING,
            };

        private:

            static struct   Texture_Data { Id id; const char * path; } textures_data[];
            static unsigned textures_count;

        private:

            static constexpr float player_speed_x = 5.f;

        private:

            State          state;
            Gameplay_State gameplay;
            bool           paused;

            unsigned       canvas_width;
            unsigned       canvas_height;

            Texture_Map    textures;
            Sprite_List    sprites;

            Sprite  * rulesText1;
            Sprite  * backButton1;
            Sprite  * ground1;
            Sprite  * background1;

        public:

            Rules_Scene();

            basics::Size2u get_view_size () override
            {
                return { canvas_width, canvas_height };
            }

            float x;
            float y;

            bool initialize () override;
            void suspend    () override;
            void resume     () override;

            void handle     (basics::Event & event) override;
            void update     (float time) override;
            void render     (Context & context) override;

        private:

            void load_textures    ();
            void create_sprites   ();

            void restart_game     ();
            void start_playing    ();

            void run_simulation   (float time);

            void render_loading   (Canvas & canvas);
            void render_scene     (Canvas & canvas);


        };

    }

#endif
